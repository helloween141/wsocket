var WebSocketServer = new require('ws');
var Client = require('./client');

var Server = function (messageProtocol, port) {
    port = port || 3010;

    this.clients = [];

    this.messageProtocol = messageProtocol;

    this.ws = new WebSocketServer.Server({port: port});

    /**
     * При подключении клиента инициализируем его и засунем в общий список пользователей
     */
    this.ws.on('connection', (function (connection) {
        var id = this.getFreeID();

        this.clients[id] = new Client({id: id, connection: connection, server: this});
        /**
         * При получении сообщения вызовем handler для этого сообщения у пользователя
         */
        connection.on('message', (function (message) {
            var client = this.clients[id];
            var data = this.messageProtocol.decode(message);
            var method = data.shift() + 'Handler';
            client[method].apply(client, data);
        }).bind(this));

        /**
         * При закрытии подключения вызовем destroy пользователя и удалим ссылку на него
         */
        connection.on('close', (function () {
            this.clients[id].destroy();
            delete this.clients[id];
            this.sendToAll('logout', id);
        }).bind(this));
    }).bind(this));

    /**
     * Ищет свободную ячейку в массиве clients, занимает её и возвращает её номер
     *
     * @returns {number}
     */
    this.getFreeID = function () {
        var i = 0;
        while(typeof this.clients[i] !== 'undefined') {
            i++;
        }
        this.clients[i] = true;
        return i;
    };

    /**
     * Отправить данные пользователю
     *
     * @param {Client} client
     * @param {string} event
     * @param {*} [data]
     */
    this.send = function (client, event, data) {
        if (typeof data !== 'undefined') {
            data = [event].concat(data);
        } else {
            data = [event];
        }

        var message = this.messageProtocol.encode(data);
        client.connection.send(message);
    };

    /**
     * Отправить данные всем пользователям
     *
     * @param {string} event
     * @param {*} [data]
     * @param {*} [excludes] - идентификаторы пользователей, которых нужно исключить из рассылки
     */
    this.sendToAll = function (event, data, excludes) {
        excludes = excludes || [];
        if (excludes.constructor !== Array)
            excludes = [excludes];

        var len = this.clients.length;
        for (var i = 0; i < len; i++) {
            if (typeof this.clients[i] === 'undefined') continue;
            if (excludes.indexOf(this.clients[i].id) >= 0 || excludes.indexOf(this.clients[i]) >= 0) continue;

            this.send(this.clients[i], event, data);
        }
    };
};

module.exports = Server;