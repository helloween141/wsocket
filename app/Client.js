/**
 * Клиент
 *
 * @param {Object} options
 * @param {number} options.id - идентификатор клиента
 * @param {WebSocket} options.connection - подключение клиента по WS
 * @param {Server} options.server - указатель на сервер
 * @constructor
 */
var Client = function (options) {
    /**
     * Идентификатор клиента
     *
     * @type {number}
     */
    this.id = options.id;

    /**
     * Подключение клиента по WS
     *
     * @type {WebSocket}
     */
    this.connection = options.connection;

    /**
     * Указатель на сервер
     *
     * @type {Server}
     */
    this.server = options.server;

    /**
     * Информация о игроке:
     * id: id
     * name: имя
     * position: текущая позиция {x: <value>, y: <value>}
     * @type {Object}
     */
    this.playerInfo = {};

    /**
     * Деструктор
     */
    this.destroy = function () {
        options = null;
        this.server = null;
    };

    /**
     * Событие на залогинивание пользователя
     */
    this.loginHandler = function () {
        this.playerInfo = {
            id: this.id,
            name: this.setNickname(),
            position: this.setPosition()
        };

        this.server.sendToAll('login', this.getClientInfo());
    };

    this.gameInfoHandler = function () {
        for (var i = 0; i < this.server.clients.length; i++) {
            this.server.sendToAll('gameInfo', this.server.clients[i].playerInfo);
        }
    };

    /**
     * Обработчик нажатия на клавишу
     *
     */
    this.keyPressedHandler = function (key) {
        switch (key) {
            // W
            case 119:
                this.playerInfo.position.y -= 10;
                this.playerInfo.vector = 3;
                this.playerInfo.isMoving = true;
                break;
            // S
            case 115:
                this.playerInfo.position.y += 10;
                this.playerInfo.vector = 2;
                this.playerInfo.isMoving = true;
                break;
            // A
            case 97:
                this.playerInfo.position.x -= 10;
                this.playerInfo.vector = 0;
                this.playerInfo.isMoving = true;
                break;
            // D
            case 100:
                this.playerInfo.position.x += 10;
                this.playerInfo.vector = 1;
                this.playerInfo.isMoving = true;
                break;
        }
        this.server.sendToAll('gameInfo', this.playerInfo);
    };

    /**
     * Получает информацию про игрока
     *
     * @returns {string}
     */
    this.getClientInfo = function () {
        return this.playerInfo;
    };

    /**
     * Устанавливает имя игрока
     *
     * @returns {string}
     */
    this.setNickname = function () {
        return 'Bro#' + (this.id + 1);
    };

    /**
     * Устанавливает стартовую позицию игрока
     *
     * @returns {string}
     */
    this.setPosition = function () {
        return {
           x: Math.floor(Math.random() * (600 - 480)) + 480,
           y: Math.floor(Math.random() * (100)) + 180
        };
    }

};

module.exports = Client;