/**
 * MessageProtocol, работающий с JSON форматом
 * @constructor
 */
var MessageProtocol = function () {
    /**
     * {@inheritDoc}
     */
    this.encode = function (data) {
        return JSON.stringify(data);
    };

    /**
     * {@inheritDoc}
     */
    this.decode = function (data) {
        return JSON.parse(data);
    };
};

module.exports = MessageProtocol;
