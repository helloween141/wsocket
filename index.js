var MessageProtocol = require('./app/MessageProtocol');
var SocketServer = require('./app/Server');

new SocketServer(new MessageProtocol());


/**
 * Бросим слушатель статики на 3000 порт (вся статика в public)
 */
var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');
var http = require('http');
var serve = serveStatic('./public');
var server = http.createServer(function (req, res) {
    var done = finalhandler(req, res);
    serve(req, res, done);
});
server.listen(3000);