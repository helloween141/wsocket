/**
 * Классы StaticSprite и AnimatedSprite - статический/анимированный спрайт
 *
 * @param options
 * Для статического спрайта:
 * src - путь к изображению
 * position - координаты изображения {x: 0, y: 0}
 *
 * Для анимированного спрайта:
 * isMoving - флаг движения
 * vector - индекс блока (бывает, что в спрайте несколько блоков)
 * ticksPerFrame - кол-во тиков, необходимое для смены кадра
 * numberOfFrames - кол-во кадров
 *
 * @constructor
 */

var StaticSprite = function(options) {
    this.image = new Image();
    this.image.src = options.src;
    this.position = options.position || {x: 0, y: 0};

    /**
     * Отрисовка спрайта
     */
    this.draw = function (context) {
        context.drawImage(this.image,
            0, // sx
            0, // sy
            this.image.width, // sw
            this.image.height, // sh
            this.position.x,
            this.position.y,
            this.image.width,
            this.image.height);
    };
};

var AnimatedSprite = function(options) {
    StaticSprite.apply(this, arguments);

    this.isMoving = options.isMoving || false;
    this.vector = options.vector || 0;
    this.numberOfFrames = options.numberOfFrames || 4;
    this.ticksPerFrame = options.ticksPerFrame || 4;
    this.tickCount = 0;
    this.frameIndex = 0;

    /**
     * Обновление спрайта
     */
    this.update = function() {
        if (this.isMoving) {
            this.tickCount += 0.5;
            if (this.tickCount > this.ticksPerFrame) {
                this.tickCount = 0;
                if (this.frameIndex < this.numberOfFrames - 1) {
                    this.frameIndex += 1;
                } else {
                    this.frameIndex = 0;
                }
            }
        }
    };

    /**
     * Отрисовка спрайта
     */
    this.draw = function(context) {
        context.drawImage(this.image,
            this.frameIndex * this.image.width / this.numberOfFrames, // sx
            this.image.height / this.numberOfFrames * this.vector, // sy
            this.image.width / this.numberOfFrames, // sw
            this.image.height / this.numberOfFrames, // sh
            this.position.x,
            this.position.y,
            this.image.width / this.numberOfFrames,
            this.image.height / this.numberOfFrames);
    };

};
