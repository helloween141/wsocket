/**
 * Класс Player -  представление игрока
 * id - id
 * name - ник игрока
 * position - позиция игрока
 * sprite - спрайт игрока
 * @constructor
 */
var Player = function(id, name, position) {
    
    this.id = id;

    this.name = name;

    this.position = position;

    this.sprite = new AnimatedSprite({
        src: '../resources/sprite.png',
        position: this.position,
        isMoving: true,
        vector: 0,
        numberOfFrames: 4,
        ticksPerFrame: 4
    });

    this.update = function () {
        this.sprite.update();
        this.sprite.isMoving = false;
    };
    
    this.draw = function (gctx) {
        this.sprite.draw(gctx);
    };
};
