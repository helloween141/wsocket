/**
 * Класс реализующий сокетное соединение с сервером
 */
var SocketConnection = function () {
    /**
     * Сокетное подключение
     * @type {null|WebSocket}
     * @private
     */
    this._ws = null;

    /**
     * Объект, содержащий в себе обработчики сообщений от сервера
     * @type {object}
     * @private
     */
    this._e = {};

    /**
     * Открывает сокетное соединение
     *
     * @param {string} [host=location.hostname] - хост для сокетного подключения
     * @param {number} [port=3000] - порт сокетного подключения
     * @param {function} [callback] - вызовется, когда соединение будет установлено
     */
    this.open = function (host, port, callback) {
        host = host || location.hostname;
        port = port || 3010;

        this._ws = new WebSocket('ws://' + host + ':' + port + '/');

        this._ws.onopen = (function () {
            this.trigger('open');
            if (typeof callback === 'function') callback();
        }).bind(this);

        this._ws.onmessage = (function (message) {
            var data = JSON.parse(message.data);
            this.trigger(data.shift(), data);
        }).bind(this);

        this._ws.onclose = (function () {
            this.close();
        }).bind(this);
    };

    /**
     * Отправляет данные на сервер
     *
     * @param {string} event - действие, которое необходимо обработать на сервере
     * @param {string} [data] - дополнительные данные
     */
    this.send = function (event, data) {
        if (typeof data !== 'undefined') {
            data = [event].concat(data);
        } else {
            data = [event];
        }

        this._ws.send(JSON.stringify(data));
    };

    /**
     * Закрывает сокетное соединение
     */
    this.close = function () {
        if (this._ws === null) return;

        this.trigger('close');
        this._ws.close();
        this._ws = null;
    };

    /**
     * Добавляет handler для обработки сообщения от сервера
     *
     * @param {string} event - ключ сообщения
     * @param {function} handler - обработчик
     * @param {int} [count=Infinity] - необходимое количество вызовов, после указанного количества вызовов обработчик будет удалён
     */
    this.on = function (event, handler, count) {
        if (typeof this._e[event] === 'undefined') this._e[event] = [];
        this._e[event].push({handler: handler.bind(this), count: count || Infinity});
    };

    /**
     * Добавляет handler для обработки сообщения от сервера, который будет вызван лишь раз
     *
     * @param {string} event - ключ сообщения
     * @param {function} handler - обработчик
     */
    this.once = function (event, handler) {
        this.on(event, handler, 1);
    };

    /**
     * Удаляет handler обрабатывающий сообщения от сервера
     * Если handler не указан, то удалит все обработчики для указанного event
     *
     * @param {string} event - ключ сообщения
     * @param {function} [handler] - обработчик
     */
    this.off = function (event, handler) {
        if (typeof this._e[event] === 'undefined') return;

        if (typeof handler === 'undefined') {
            delete this._e[event];
            return;
        }

        var len = this._e[event].length;
        for (var i = 0; i < len; i++) {
            if (this._e[event][i].handler === handler) {
                this._deleteEvent(event, i);
                return;
            }
        }
    };

    /**
     * Вызывает все handler'ы для указанного event
     *
     * @param {string} event - ключ сообщения от сервера
     * @param {*} [data] - данные, которые необходимо передать в handler'ы
     */
    this.trigger = function (event, data) {
        if (typeof this._e[event] === 'undefined') return;

        var len = this._e[event].length;
        for (var i = 0; i < len; i++) {
            this._e[event][i].handler.apply(this, data);
            if (--this._e[event][i].count <= 0)
                this._deleteEvent(event, i);
        }
    };

    /**
     * Удаляет i-тый handler для указанного event
     *
     * @param {string} event
     * @param {number} i
     * @private
     */
    this._deleteEvent = function (event, i) {
        this._e[event].splice(i, 1);
        if (this._e[event].length === 0)
            delete this._e[event];
    };
};