window.onload = function () {

    var connection = new SocketConnection();

    var game = new Game();

    createCanvas();

    /**
     * Когда кто-то залогинился
     */
    connection.on('login', function (playerInfo) {
        console.log(playerInfo.name + ' entered!');
    });

    /**
     * Когда кто-то вышел
     */
    connection.on('logout', function (id) {
        game.players.splice(id, 1);
    });

    /**
     * Получаем информацию о состоянии игры
     */
    connection.on('gameInfo', function (playersInfo) {
        var id = playersInfo.id;
        if (!game.players[id]) {
            game.players[id] = new Player(id, playersInfo.name, playersInfo.position);
        } else {
            game.players[id].sprite.position = playersInfo.position;
            game.players[id].sprite.vector = playersInfo.vector;
            game.players[id].sprite.isMoving = playersInfo.isMoving;

        }
    });

    /**
     * Событие при нажатии клавиши
     */
    $(document).keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        connection.send('keyPressed', keycode);
    });

    /**
     * Открываем сокетное соединение
     */
    connection.once('open', function () {
        connection.send('login');
        connection.send('gameInfo');
    });
    connection.open();

    setInterval(function() {
        game.update();
        game.render();
    }, game.tickTime);

};