/*
 *   Класс Game
*/
var Game = function() {

    this.tickTime = 1000/60; // Время обновления игрового цикла

    this.players = [];

    // Общий игровой цикл
    this.update = function() {
        for (var i = 0; i < this.players.length; i++) {
            this.players[i].update();
        }
    };

    this.render = function() {
        // Отрисовка поля
        gctx.fillStyle = "rgb(200, 120, 100)";
        gctx.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        gctx.fillStyle = "rgb(0, 0, 0)";

        for (var i = 0; i < this.players.length; i++) {
            gctx.font = "20px Georgia";
            gctx.fillText(this.players[i].name, this.players[i].sprite.position.x, this.players[i].sprite.position.y);

            this.players[i].draw(gctx);
        }
    };

};
